//
//  Constants.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

let kWidth = UIScreen.main.bounds.width
let kHeight = UIScreen.main.bounds.height
let gothamRoundedMedium = "GothamRounded-Medium"
let gothamRoundedBold = "GothamRounded-Bold"
let gothamRoundedBook = "GothamRounded-Book"

enum UserPlan: String {
    case premium = "premium"
    case freemium = "freemium"
}


