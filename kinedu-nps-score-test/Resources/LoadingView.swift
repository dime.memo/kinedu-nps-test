//
//  LoadingView.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: NSObject {
    
    static let shared = LoadingView()
    
    let containerView: UIView = {
        let _view = UIView()
        _view.backgroundColor = .white
        return _view
    }()
    
    let animationView: LOTAnimationView = {
        let _view = LOTAnimationView(name: "balls")
        _view.loopAnimation = true
        _view.translatesAutoresizingMaskIntoConstraints = false
        return _view
    }()
    
    fileprivate func setupLoadingView() {
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        keyWindow.addSubview(containerView)
        containerView.frame = keyWindow.bounds
        containerView.addSubview(animationView)
        NSLayoutConstraint.activate([
            animationView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            animationView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            animationView.widthAnchor.constraint(equalTo: containerView.widthAnchor,
                                                 constant: -40),
            animationView.heightAnchor.constraint(equalTo: containerView.widthAnchor,
                                                  constant: -40)
            ])        
        animationView.play()
    }
    
    func show() {
        setupLoadingView()
    }
    
    func hide() {
        DispatchQueue.main.async {
            self.animationView.pause()
            UIView.animate(withDuration: 0.3, animations: {
                self.containerView.alpha = 0
            }) { (_) in
                self.containerView.removeFromSuperview()
            }
        }
    }
}

