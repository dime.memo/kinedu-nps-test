//
//  NavigationController+Additions.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

extension UIColor {
    // Permite poner colores en codigo HEX comenzando con "0x"
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIView {
    func addSubviews(views: [UIView]) {
        views.forEach { addSubview($0) }
    }
}

extension Int {
    func toDouble() -> Double {
        return Double(self)
    }
}

extension Double {
    func toInt() -> Int {
        return Int(self)
    }
}

extension UIView {
    // Redondea esquinas especificas de un UIView
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }
}

extension UIViewController {
    // Obtiene el tipo de dipositivo en el que se encuentra (iPhone ó iPad)
    func getDevice() -> UIUserInterfaceIdiom {
        return UIDevice.current.userInterfaceIdiom
    }
}
