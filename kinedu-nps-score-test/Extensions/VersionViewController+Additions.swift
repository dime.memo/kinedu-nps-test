//
//  VersionViewController+Additions.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/6/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

extension VersionViewController {
    
    // Esta funcion elimina los valores repetidos de un objeto (se utiliza para obtener las versiones de la app)
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    // Crea una imagen del un color especificado
    func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.3, height: 1.3)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
