//
//  DetailViewController+Additions.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

extension DetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        ImpactFeedbackService.shared.impactFeedbackGenerator(.light)
        guard let cell = collectionView.cellForItem(at: indexPath) as? DetailCollectionViewCell else { return }
        cell.focus()
        updateUsersLabels(filterNPSScores(by: UserPlan.premium.rawValue, nps: indexPath.row).count, freemium: filterNPSScores(by: UserPlan.freemium.rawValue, nps: indexPath.row).count)
        getActivityViewsPer(answer: indexPath.row)    
        guard getDevice() == .phone else { return }
        collectionView.scrollToItem(at: indexPath,
                                    at: .centeredHorizontally,
                                    animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DetailCollectionViewCell else { return }
        
        cell.blur()
    }
}

extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell",
                                                      for: indexPath) as! DetailCollectionViewCell
        
        cell.babyImageView.image = UIImage(named: "baby_\(indexPath.row)")
        cell.babyLabel.text = "\(indexPath.row)"
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let currentRow = centeredCollectionViewFlowLayout.currentCenteredPage,
            let cell = collectionView.cellForItem(at: IndexPath(item: currentRow, section: 0)) as? DetailCollectionViewCell else { return }
        cell.focus()
        updateUsersLabels(filterNPSScores(by: UserPlan.premium.rawValue, nps: currentRow).count, freemium: filterNPSScores(by: UserPlan.freemium.rawValue, nps: currentRow).count)
        getActivityViewsPer(answer: currentRow)
        
        // Esta parte previene que se queden dos celdas seleccionas cuando hace scroll y luego selecciona una celda diferente.
        collectionView.selectItem(at: IndexPath(row: currentRow, section: 0), animated: false, scrollPosition: .centeredHorizontally)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        guard let currentRow = centeredCollectionViewFlowLayout.currentCenteredPage,
            let cell = collectionView.cellForItem(at: IndexPath(item: currentRow, section: 0)) as? DetailCollectionViewCell else { return }
        
        cell.blur()
    }
}

