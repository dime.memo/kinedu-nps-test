//
//  NetworkManager.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/8/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import Foundation
import Reachability

class NetworkService: NSObject {
    
    var reachability: Reachability!
    
    static var isConnected = false
    static let shared = NetworkService()
    
    private override init() {
        super.init()
        
        reachability = Reachability()!
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Notificacion para realizar cambio globales en la app
    }
    
    static func stopNotifier() -> Void {
        do {
            try (NetworkService.shared.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    static func isReachable(completed: @escaping (NetworkService) -> Void) {
        if (NetworkService.shared.reachability).connection != .none {
            completed(NetworkService.shared)
        }
    }
    
    static func isUnreachable(completed: @escaping (NetworkService) -> Void) {
        if (NetworkService.shared.reachability).connection == .none {
            completed(NetworkService.shared)
        }
    }
    
    static func isReachableViaWWAN(completed: @escaping (NetworkService) -> Void) {
        if (NetworkService.shared.reachability).connection == .cellular {
            completed(NetworkService.shared)
        }
    }
    
    static func isReachableViaWiFi(completed: @escaping (NetworkService) -> Void) {
        if (NetworkService.shared.reachability).connection == .wifi {
            completed(NetworkService.shared)
        }
    }
}
