//
//  APIServices.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit
import Alamofire

enum Endpoint: String {
    case nps = "http://demo.kinedu.com/bi/nps"
}

class APIService: NSObject {
    
    private override init () { }
    
    static let shared = APIService()
    
    func getNPS(_ completion: @escaping ([NPSScore]?, String?) -> Void) {
        Alamofire.request(Endpoint.nps.rawValue).responseJSON { (response) in
            guard response.error == nil else {
                completion(nil, response.error?.localizedDescription)
                return
            }
            guard let data = response.data else {
                completion(nil, "An error occurred, try again.")
                return
            }
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let npsScores = try decoder.decode([NPSScore].self, from: data)
                completion(npsScores, nil)
            } catch let jsonError {
                completion(nil, jsonError.localizedDescription)
            }
        }
    }
}
