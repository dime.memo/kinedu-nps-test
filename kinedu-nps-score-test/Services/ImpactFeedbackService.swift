//
//  ImpactFeedbackService.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/8/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class ImpactFeedbackService: NSObject {
    
    private override init () {}
    
    static let shared = ImpactFeedbackService()
    
    func notificationFeedbackGenerator(_ notificationType: UINotificationFeedbackGenerator.FeedbackType) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(notificationType)
    }
    
    func impactFeedbackGenerator(_ style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let generator = UIImpactFeedbackGenerator(style: style)
        generator.prepare()
        generator.impactOccurred()
    }
    
    func selectionFeedbackGenerator() {
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
    }
}
