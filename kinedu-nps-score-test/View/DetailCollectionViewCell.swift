//
//  DetailCollectionViewCell.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var babyImageView: UIImageView!
    @IBOutlet weak var babyLabel: UILabel!
    
    func focus() {
        babyLabel.font = UIFont(name: gothamRoundedBold, size: 24)
        babyImageView.layer.cornerRadius = babyImageView.frame.width / 2
        babyImageView.layer.borderWidth = 4
        babyImageView.layer.borderColor = UIColor.white.cgColor
    }
    
    func blur() {
        babyLabel.font = UIFont(name: gothamRoundedMedium, size: 22)
        babyImageView.layer.cornerRadius = babyImageView.frame.width / 2
        babyImageView.layer.borderWidth = 0
    }
}
