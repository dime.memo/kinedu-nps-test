//
//  DetailCellCustomView.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class DetailCellCustomView: UIImageView {
    
    @IBInspectable var borderColor: UIColor {
        get {
            return .white
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var clipsBounds: Bool {
        get {
            return self.clipsToBounds
        }
        set {
            self.clipsToBounds = newValue
        }
    }
}

