//
//  NoInternetConnection.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class NoNetworkConnection: UIView {

    let whoopsLabel = UILabel()
    let messageLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView() {
        
        backgroundColor = UIColor(rgb: 0xF8F8F8)
        
        let containerStack = UIStackView()
        containerStack.translatesAutoresizingMaskIntoConstraints = false
        containerStack.axis = .vertical
        containerStack.spacing = 10
        
        whoopsLabel.font = UIFont(name: gothamRoundedBold, size: 24)
        whoopsLabel.text = "W H O O P S"
        whoopsLabel.textAlignment = .center
        whoopsLabel.textColor = UIColor(rgb: 0x4A4A4A)
        
        messageLabel.font = UIFont(name: gothamRoundedMedium, size: 16)
        messageLabel.textAlignment = .center
        messageLabel.textColor = UIColor(rgb: 0x4A4A4A)
        messageLabel.numberOfLines = 0
        
        containerStack.addArrangedSubview(whoopsLabel)
        containerStack.addArrangedSubview(messageLabel)
        
        addSubview(containerStack)
        
        NSLayoutConstraint.activate([
            containerStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerStack.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerStack.widthAnchor.constraint(equalTo: widthAnchor, constant: -40),
            ])
        
    }
}
