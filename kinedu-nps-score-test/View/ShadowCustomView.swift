//
//  ShadowCustomView.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class ShadowCustomView: UIView {
    
    @IBInspectable var shadowColor: UIColor {
        get {
            return .gray
        }
        set {
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return self.layer.shadowRadius
        }
        set {
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}
