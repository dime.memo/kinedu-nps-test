//
//  Build.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import Foundation

struct Build: Decodable {
    let version: String
    let releaseDate: String
}
