//
//  NPSScore.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import Foundation

struct NPSScore: Decodable, Hashable {

    let id: Int
    let nps: Int
    let daysSinceSignup: Int
    let userPlan: String
    let activityViews: Int
    let build: Build
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(build.version)
    }
    
    static func == (lhs: NPSScore, rhs: NPSScore) -> Bool {
        return lhs.build.version == rhs.build.version
    }
}
