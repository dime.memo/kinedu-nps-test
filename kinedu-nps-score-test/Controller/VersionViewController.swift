//
//  ViewController.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/5/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit

class VersionViewController: UIViewController {
    
    @IBOutlet weak var versionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var fremiumTotalLabel: UILabel!
    @IBOutlet weak var premiumTotalLabel: UILabel!
    @IBOutlet weak var totalFremiumUsersLabel: UILabel!
    @IBOutlet weak var totalPremiumUsersLabel: UILabel!
    
    var allAppVersions = [String]()
    var npsScores = [NPSScore]()
    var noInternetConnection: NoNetworkConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewDidLoad()
        configureSegementedControl()
        checkNetworkConnection()
        configureListenerForNetworkConnectionChanges()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "detailSegue",
            let detailViewController = segue.destination as? DetailViewController else { return }
        let currentActiveVersion = allAppVersions[versionSegmentedControl.selectedSegmentIndex]
        detailViewController.segueNPSScores = filterNPSScore(by: currentActiveVersion)
    }
    
    fileprivate func configureViewDidLoad() {
        navigationItem.title = "Kinedu NPS Score"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
    
    fileprivate func configureErrorView(_ errorMsg: String?) {
        guard let noInternetConnectionView = noInternetConnection else {
            noInternetConnection = NoNetworkConnection(frame: view.bounds)
            noInternetConnection!.messageLabel.text = errorMsg
            view.addSubview(noInternetConnection!)
            return
        }
        noInternetConnectionView.messageLabel.text = errorMsg
    }
    
    fileprivate func configureSegementedControl() {
        versionSegmentedControl.layer.borderWidth = 1.3
        versionSegmentedControl.layer.masksToBounds = true
        versionSegmentedControl.layer.cornerRadius = 5
        versionSegmentedControl.layer.borderColor  = UIColor(rgb: 0xDADADA).cgColor
        versionSegmentedControl.setDividerImage(imageWithColor(color: UIColor(rgb: 0xDADADA)),
                                                forLeftSegmentState: .normal,
                                                rightSegmentState: .normal,
                                                barMetrics: .default)
        versionSegmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: gothamRoundedBold, size: 13)!,
            NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x1B75BB)
            ], for: .normal)
        versionSegmentedControl.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name: gothamRoundedBold, size: 13)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .selected)
    }
    
    // Listener para detectar cambios en la coneccion a internet
    fileprivate func configureListenerForNetworkConnectionChanges() {
        NetworkService.shared.reachability.whenReachable = { reachability in
            NetworkService.isConnected = true
            self.getNPSScore()
            guard let noInternetConnection = self.noInternetConnection else { return }
            noInternetConnection.removeFromSuperview()
            self.noInternetConnection = nil
        }
        
        NetworkService.shared.reachability.whenUnreachable = { reachability in
            NetworkService.isConnected = false
            self.configureErrorView("The Internet connection appears to be offline.")
        }
    }
    
    // Consulta inicial para revisar si tiene coneccion a internet
    fileprivate func checkNetworkConnection() {
        NetworkService.isReachable { (_) in
            NetworkService.isConnected = true
            self.getNPSScore()
        }
        guard !NetworkService.isConnected else { return }
        configureErrorView("The Internet connection appears to be offline.")
    }
    
    @IBAction func segmentedControlChange(_ sender: UISegmentedControl) {
        ImpactFeedbackService.shared.impactFeedbackGenerator(.light)
        configureData(npsScores: npsScores,
                      for: allAppVersions[sender.selectedSegmentIndex],
                      current: sender.selectedSegmentIndex)
    }
    
    func configureData(npsScores: [NPSScore], for version: String, current index: Int) {
        let filterByVersionNPSScores = filterNPSScore(by: version)
        let premium = self.getNPSScore(forUser: "premium", in: filterByVersionNPSScores)
        let freemium = self.getNPSScore(forUser: "freemium", in: filterByVersionNPSScores)
        
        DispatchQueue.main.async {
            self.versionSegmentedControl.removeAllSegments()
            self.allAppVersions.enumerated().forEach { self.versionSegmentedControl.insertSegment(withTitle: $0.element, at: $0.offset, animated: false) }
            self.versionSegmentedControl.selectedSegmentIndex = index
            self.fremiumTotalLabel.text = "\(freemium.0)"
            self.premiumTotalLabel.text = "\(premium.0)"
            self.fremiumTotalLabel.textColor = freemium.0 <= 69 ? UIColor(rgb: 0xE50000) : UIColor(rgb: 0x78B742)
            self.premiumTotalLabel.textColor = premium.0 >= 70 ? UIColor(rgb: 0x78B742) : UIColor(rgb: 0xE50000)
            self.totalFremiumUsersLabel.text = "out of \(freemium.1) users"
            self.totalPremiumUsersLabel.text = "out of \(premium.1) users"
            LoadingView.shared.hide()
        }
    }
    
    func filterNPSScore(by version: String) -> [NPSScore] {
        return npsScores.filter { $0.build.version == version }
    }
    
    func getNPSScore(forUser plan: String, in npsScores: [NPSScore]) -> (Int, Int) {
        let users = npsScores.filter { $0.userPlan == plan }
        let promotersNPS = users.filter { $0.nps == 9 || $0.nps == 10 }
        let detractorsNPS = users.filter { $0.nps >= 0 && $0.nps <= 6 }
        let promotersPercentage = (promotersNPS.count.toDouble() / users.count.toDouble()) * 100
        let detractorsPercentage = (detractorsNPS.count.toDouble() / users.count.toDouble()) * 100
        let overallScore = promotersPercentage - detractorsPercentage
        return (overallScore.toInt(), users.count)
    }
    
    func getNPSScore() {
        LoadingView.shared.show()
        APIService.shared.getNPS { (npsScores, errorMsg)  in
            guard errorMsg == nil else {
                LoadingView.shared.hide()
                self.configureErrorView(errorMsg)
                return
            }
            
            self.npsScores = npsScores!
            let allVersions = self.uniq(source: npsScores!)
            guard let firstVersion = allVersions.first else { return }
            self.allAppVersions.removeAll()
            allVersions.forEach { self.allAppVersions.append($0.build.version) }
            self.configureData(npsScores: npsScores!, for: firstVersion.build.version, current: 0)
        }
    }
}
