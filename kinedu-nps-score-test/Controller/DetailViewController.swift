//
//  DetailViewController.swift
//  kinedu-nps-score-test
//
//  Created by Memo Rodriguez on 6/7/19.
//  Copyright © 2019 Guillermo Rodriguez. All rights reserved.
//

import UIKit
import CenteredCollectionView

class DetailViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var statisticsLabel: UILabel!
    @IBOutlet weak var freemiumUsersLabel: UILabel!
    @IBOutlet weak var premiumUsersLabel: UILabel!
    @IBOutlet weak var blueView: UIView!
    
    var cellPercentWidth: CGFloat!
    var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    var npsScores: [NPSScore]!
    var initialIndexPath: IndexPath!
    var segueNPSScores: [NPSScore]! {
        didSet {
            npsScores = segueNPSScores
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureViewWillAppear()
        configureUIData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureViewDidAppear()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        blueView.roundCorners([.bottomRight, .bottomLeft], radius: 6)
    }
    
    fileprivate func configureViewDidLoad() {
        
        navigationItem.title =  npsScores.first == nil ? "NPS Detail" : "NPS Detail \(npsScores.first!.build.version)"
        
        cellPercentWidth = getDevice() == .phone ?  0.29 : 0.08

        // Referencia del CenteredCollectionViewFlowLayout (REQUERIDO)
        centeredCollectionViewFlowLayout = (collectionView.collectionViewLayout as! CenteredCollectionViewFlowLayout)
        
        // Velocidad de desaceleracion del scroll view (REQUERIDO)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Establecer tamaños de las celdas
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: kWidth * cellPercentWidth,
            height: getDevice() == .phone ? (kWidth * cellPercentWidth + 36) : collectionView.frame.height
        )
    }
    
    fileprivate func configureViewWillAppear() {
        initialIndexPath = getDevice() == .phone ? IndexPath(item: 0, section: 0) : IndexPath(item: 5, section: 0)
        collectionView.selectItem(at: initialIndexPath, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    fileprivate func configureViewDidAppear() {
        guard let cell = collectionView.cellForItem(at: initialIndexPath) as? DetailCollectionViewCell else { return }
        cell.focus()
    }
    
    func configureUIData() {
        updateUsersLabels(filterNPSScores(by: UserPlan.premium.rawValue, nps: initialIndexPath.row).count, freemium: filterNPSScores(by: UserPlan.freemium.rawValue, nps: initialIndexPath.row).count)
        getActivityViewsPer(answer: initialIndexPath.row)
    }
    
    func configureAttributedLabel(with userPercentage: Double, answer number: Int, activitieViews: Int) {
        let percentageAttributes = [NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x75B753),
                                    NSAttributedString.Key.font: UIFont(name: gothamRoundedBold, size: 18)!]
        let mainAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white,
                              NSAttributedString.Key.font: UIFont(name: gothamRoundedBold, size: 14)!]
        let activitiesAttributes = [NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x1FADDF),
                                    NSAttributedString.Key.font: UIFont(name: gothamRoundedBold, size: 18)!]
        let percentageAttributedString = NSMutableAttributedString(string:"\(userPercentage.toInt())%", attributes: percentageAttributes)
        let mainAttributedString = NSMutableAttributedString(string: " of the users that answered \(number) in their NPS score saw ", attributes: mainAttributes)
        let activitiesAttributedString = NSMutableAttributedString(string:"\(activitieViews) activities", attributes:activitiesAttributes)
        
        percentageAttributedString.append(mainAttributedString)
        percentageAttributedString.append(activitiesAttributedString)
        statisticsLabel.attributedText = percentageAttributedString
    }
    
    func filterNPSScores(by userPlan: String, nps: Int) -> [NPSScore] {
        return npsScores.filter { $0.userPlan == userPlan }.filter { $0.nps == nps }
    }
    
    func getActivityViewsPer(answer number: Int) {
        var activityViews = [Int]()
        let filteredArray = npsScores.filter { $0.nps == number }
        filteredArray.forEach { activityViews.append($0.activityViews) }
        let activityViewCount = activityViews.reduce(into: [:]) { counts, activityViews in counts[activityViews, default: 0] += 1 }
        guard let maximumActivityViews = activityViewCount.values.max() else { return }
        let dictionaryKey = activityViewCount.compactMap { (key, value) -> Int? in
            guard value == maximumActivityViews else { return nil }
            return key
        }
        guard let activityNumber = dictionaryKey.max() else { return }
        let userPercentage = (maximumActivityViews.toDouble() / filteredArray.count.toDouble()) * 100
        configureAttributedLabel(with: userPercentage, answer: number, activitieViews: activityNumber)
    }
    
    func updateUsersLabels(_ premium: Int, freemium: Int) {
        premiumUsersLabel.text = "\(premium)"
        freemiumUsersLabel.text = "\(freemium)"
    }
}
